const client = require('../libs/httpclient');

const baseURL = 'http://www.tuling123.com/openapi/api?key='
const tulingAPIKey = '873ba8257f7835dfc537090fa4120d14';
const baseURLTail = '&info='
const tulingURL = baseURL + tulingAPIKey + baseURLTail;

function get(str, callback) {
    client.url_get(tulingURL + encodeURI(str), (err, res, info) => {
        console.log('图灵响应:\n', info)
        let retMsgJson = JSON.parse(info)
        let retMsgTxt = retMsgJson.text
        retMsgJson.url && (retMsgTxt += '\n' + retMsgJson.url)
        retMsgJson.text === '亲，已帮你找到图片' && retMsgJson.url && (retMsgTxt = retMsgJson.url)
        callback && callback( retMsgTxt );
    });
}

module.exports = {
    getMsg: get
}